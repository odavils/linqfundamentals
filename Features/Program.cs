﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Features
{
    class Program
    {
        static void Main(string[] args)
        {
            // Return value
            Func<int, int> square = x => x * x;
            Func<int, int, int> add = (x, y)  => x + y;
            Func<int, int, int> add2 = (x, y) => 
            {
                int temp = x + y;
                return temp;
            };

            // Return void
            Action<int> write = x => Console.WriteLine(x);

            IEnumerable<Employee> developers = new Employee[]
            {
                new Employee {Id = 1, Name = "Scott"},
                new Employee {Id = 1, Name = "Chris"}
            };

            IEnumerable<Employee> sales = new List<Employee>()
            {
                new Employee {Id = 3, Name = "Alex"}
            };

            write(square(add(3, 5)));

            //foreach (var employee in developers.Where(NameStartsWithS))
            //foreach (var employee in developers.Where(
            //    delegate (Employee employee)
            //    {
            //        return employee.Name.StartsWith("S");
            //    }))
            //foreach (var employee in developers.Where(e => e.Name.StartsWith("S")))
            var query = developers.Where(e => e.Name.Length == 5)
                                               .OrderBy(e => e.Name);

            var query2 = from developer in developers
                         where developer.Name.Length == 5
                         orderby developer.Name
                         select developer;

            foreach (var employee in query2)
            {
                Console.WriteLine(employee.Name);
            }
        }

        private static bool NameStartsWithS(Employee employee)
        {
            return employee.Name.StartsWith("S");
        }
    }
}
