﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Cars
{
    class ProgramJoins
    {
        private static void Main2(string[] args)
        {
            var cars = ProcessFile("fuel.csv");
            var manufacturers = ProcessManufacturers("manufacturers.csv");


            //var query = from car in cars
            //            group car by car.Manufacturer.ToUpper() into manufacturer
            //            orderby manufacturer.Key
            //            select manufacturer;
            var query = from car in cars
                        group car by car.Manufacturer into carGroup
                        select new
                        {
                            Name = carGroup.Key,
                            Max = carGroup.Max(c => c.Combined),
                            Min = carGroup.Min(c => c.Combined),
                            Avg = carGroup.Average(c => c.Combined)
                        } into result
                        orderby result.Max descending
                        select result;

            //var query2 = cars.GroupBy(c => c.Manufacturer.ToUpper())
            //                 .OrderBy(g => g.Key);
            var query2 = cars.GroupBy(c =>c.Manufacturer)
                             .Select(g => 
                             {
                                 var results = g.Aggregate(new CarStatistics(),
                                                     (acc, c) => acc.Accumulate(c),
                                                     acc => acc.Compute());
                                 return new
                                 {
                                     Name = g.Key, 
                                     Avg = results.Average,
                                     results.Min,
                                     results.Max
                                 };
                             })
                             .OrderByDescending(r => r.Max);

            var query3 = manufacturers.GroupJoin(cars,
                                                 m => m.Name,
                                                 c => c.Manufacturer,
                                                 (m, g) => new
                                                 {
                                                     Manufacturer = m,
                                                     Cars = g
                                                 })
                                       .GroupBy(m => m.Manufacturer.Headquarters);

            //foreach (var group in query)
            //{
            //    //Console.WriteLine($"{result.Key} has {result.Count()} cars");
            //    Console.WriteLine(group.Key);
            //    foreach (var car in group.OrderByDescending(c => c.Combined).Take(2))
            //    {
            //        Console.WriteLine($"\t{car.Name} : {car.Combined}");
            //    }
            //}

            //foreach (var group in query2)
            //{
            //    //Console.WriteLine($"{result.Key} has {result.Count()} cars");
            //    Console.WriteLine($"{group.Manufacturer.Name}:{group.Manufacturer.Headquarters}");
            //    foreach (var car in group.Cars.OrderByDescending(c => c.Combined).Take(2))
            //    {
            //        Console.WriteLine($"\t{car.Name} : {car.Combined}");
            //    }
            //}

            foreach (var result in query2)
            {
                Console.WriteLine($"{result.Name}");
                Console.WriteLine($"\t Max: {result.Max}");
                Console.WriteLine($"\t Min: {result.Min}");
                Console.WriteLine($"\t Avg: {result.Avg}");
            }
        }

        private static List<Manufacturer> ProcessManufacturers(string path)
        {
            return File.ReadAllLines(path)
                       .Where(line => line.Length > 1)
                       .Select(line =>
                       {
                           var columns = line.Split(',');
                           return new Manufacturer
                           {
                               Name = columns[0],
                               Headquarters = columns[1],
                               Year = int.Parse(columns[2])
                           };
                       }).ToList();
        }

        private static List<Car> ProcessFile(string path)
        {
            return File.ReadAllLines(path)
                       .Skip(1)
                       .Where(line => line.Length > 1)
                       //.Select(Car.ParseFromCSV).ToList();
                       .ToCar().ToList();
        }
    }
}
