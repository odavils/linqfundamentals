﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cars
{
    class ProgramOld
    {
        private static void Main2(string[] args)
        {
            var cars = ProcessFile("fuel.csv");
            var manufacturers = ProcessManufacturers("manufacturers.csv");

            var query = cars.Where(c => c.Manufacturer == "BMW" && c.Year == 2016)
                            .OrderByDescending(c => c.Combined)
                            .ThenBy(c => c.Name);

            //var result = cars.Any(c => c.Manufacturer == "Ford");

            var query2 = from car in cars
                         join manufacturer in manufacturers
                            on new { car.Manufacturer, car.Year }
                                equals new { Manufacturer = manufacturer.Name, manufacturer.Year }
                         orderby car.Combined descending, car.Name ascending
                         select new
                         {
                             manufacturer.Headquarters,
                             car.Name,
                             car.Combined
                         };

            var query3 = cars.Join(manufacturers,
                                   c => new { c.Manufacturer, c.Year },
                                   m => new { Manufacturer = m.Name, m.Year },
                                   (c, m) => new
                                   {
                                       m.Headquarters,
                                       c.Name,
                                       c.Combined
                                   })
                             .OrderByDescending(c => c.Combined)
                             .ThenBy(c => c.Name);

            //var result = cars.SelectMany(c => c.Name);
            //var result = cars.All(c => c.Manufacturer == "Ford");

            //Console.WriteLine(result);

            foreach (var car in query3.Take(10))
            {
                Console.WriteLine($"{car.Headquarters} {car.Name} : {car.Combined}");
            }

        }

        private static void Main3(string[] args)
        {
            var cars = ProcessFile("fuel.csv");
            var manufacturers = ProcessManufacturers("manufacturers.csv");


            //var query = from car in cars
            //            group car by car.Manufacturer.ToUpper() into manufacturer
            //            orderby manufacturer.Key
            //            select manufacturer;
            var query = from manufacturer in manufacturers
                        join car in cars on manufacturer.Name equals car.Manufacturer
                            into carGroup
                        select new
                        {
                            Manufacturer = manufacturer,
                            Cars = carGroup
                        } into result
                        group result by result.Manufacturer.Headquarters;

            //var query2 = cars.GroupBy(c => c.Manufacturer.ToUpper())
            //                 .OrderBy(g => g.Key);
            var query2 = manufacturers.GroupJoin(cars,
                                                 m => m.Name,
                                                 c => c.Manufacturer,
                                                 (m, g) => new
                                                 {
                                                     Manufacturer = m,
                                                     Cars = g
                                                 })
                                      .OrderBy(m => m.Manufacturer.Name);

            var query3 = manufacturers.GroupJoin(cars,
                                                 m => m.Name,
                                                 c => c.Manufacturer,
                                                 (m, g) => new
                                                 {
                                                     Manufacturer = m,
                                                     Cars = g
                                                 })
                                       .GroupBy(m => m.Manufacturer.Headquarters);

            //foreach (var group in query)
            //{
            //    //Console.WriteLine($"{result.Key} has {result.Count()} cars");
            //    Console.WriteLine(group.Key);
            //    foreach (var car in group.OrderByDescending(c => c.Combined).Take(2))
            //    {
            //        Console.WriteLine($"\t{car.Name} : {car.Combined}");
            //    }
            //}

            //foreach (var group in query2)
            //{
            //    //Console.WriteLine($"{result.Key} has {result.Count()} cars");
            //    Console.WriteLine($"{group.Manufacturer.Name}:{group.Manufacturer.Headquarters}");
            //    foreach (var car in group.Cars.OrderByDescending(c => c.Combined).Take(2))
            //    {
            //        Console.WriteLine($"\t{car.Name} : {car.Combined}");
            //    }
            //}

            foreach (var group in query3)
            {
                //Console.WriteLine($"{result.Key} has {result.Count()} cars");
                Console.WriteLine($"{group.Key}");
                foreach (var car in group.SelectMany(g => g.Cars).OrderByDescending(c => c.Combined).Take(10))
                {
                    Console.WriteLine($"\t{car.Name} : {car.Combined}");
                }
            }
        }

        private static List<Manufacturer> ProcessManufacturers(string path)
        {
            return File.ReadAllLines(path)
                       .Where(line => line.Length > 1)
                       .Select(line =>
                       {
                           var columns = line.Split(',');
                           return new Manufacturer
                           {
                               Name = columns[0],
                               Headquarters = columns[1],
                               Year = int.Parse(columns[2])
                           };
                       }).ToList();
        }

        private static List<Car> ProcessFile(string path)
        {
            return File.ReadAllLines(path)
                       .Skip(1)
                       .Where(line => line.Length > 1)
                       //.Select(Car.ParseFromCSV).ToList();
                       .ToCar().ToList();
        }
    }
}
