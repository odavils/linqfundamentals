﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Cars
{
    class ProgramXML
    {
        private static void Main1(string[] args)
        {
            CreateXML();
            QueryXML();
        }

        private static void QueryXML()
        {
            var document = XDocument.Load("fuel.xml");
            var ns = (XNamespace) "http://pluralsight.com/cars/2016";
            var ex = (XNamespace) "http://pluralsight.com/cars/2016/ex";

            var query =
                from element in document.Element(ns + "Cars")?.Elements(ex + "Car") ?? Enumerable.Empty<XElement>()
                where element.Attribute("Manufacturer")?.Value == "BMW"
                select element.Attribute("Name").Value;

            var query2 = (document.Element(ns + "Cars")?.Elements(ex + "Car") ?? Enumerable.Empty<XElement>())
                                 .Where(e => e.Attribute("Manufacturer")?.Value == "BMW")
                                 .Select(e => e.Attribute("Name").Value);

            foreach (var name in query)
            {
                Console.WriteLine(name);
            }

        }

        private static void CreateXML()
        {
            var records = ProcessCars("fuel.csv");
            var ns = (XNamespace) "http://pluralsight.com/cars/2016";
            var ex = (XNamespace) "http://pluralsight.com/cars/2016/ex";
            var document = new XDocument();
            var cars = new XElement(ns + "Cars",
                        from record in records
                        select new XElement(ex + "Car",
                                      new XAttribute("Name", record.Name),
                                      new XAttribute("Combined", record.Combined), 
                                      new XAttribute("Manufacturer", record.Manufacturer)));

            cars.Add(new XAttribute(XNamespace.Xmlns + "ex", ex));
            document.Add(cars);
            document.Save("fuel.xml");
        }

        private static List<Manufacturer> ProcessManufacturers(string path)
        {
            return File.ReadAllLines(path)
                       .Where(line => line.Length > 1)
                       .Select(line =>
                       {
                           var columns = line.Split(',');
                           return new Manufacturer
                           {
                               Name = columns[0],
                               Headquarters = columns[1],
                               Year = int.Parse(columns[2])
                           };
                       }).ToList();
        }

        private static List<Car> ProcessCars(string path)
        {
            return File.ReadAllLines(path)
                       .Skip(1)
                       .Where(line => line.Length > 1)
                       //.Select(Car.ParseFromCSV).ToList();
                       .ToCar().ToList();
        }
    }
}
