﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;

namespace Cars
{
    class Program
    {
        static void Main(string[] args)
        {
            //
            //Func<int, int> square = x => x * x;
            //Expression<Func<int, int, int>> add = (x, y) => x + y;
            //Func<int, int, int> addI = add.Compile();

            //var result = addI(3, 5);
            //Console.WriteLine(result);
            //Console.WriteLine(add);
            //                                                

            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CarDb>());
            InsertData();
            QueryData();

        }

        private static void QueryData()
        {
            var db = new CarDb();
            db.Database.Log = Console.WriteLine;

            //var query = from car in db.Cars
            //            orderby car.Combined descending, car.Name ascending
            //            select car;

            //var query2 = 
            //    db.Cars.Where(c => c.Manufacturer == "BMW")
            //    .OrderByDescending(c => c.Combined)
            //    .ThenBy(c => c.Name)
            //    .Take(10)
            //    .ToList();

            var query3 =
                db.Cars.GroupBy(c => c.Manufacturer)
                .Select(g => new
                {
                    Name = g.Key,
                    Cars = g.OrderByDescending(c => c.Combined).Take(2)
                });

            var query4 =
                from cars in db.Cars
                group cars by cars.Manufacturer into grouped
                select new
                {
                    Name = grouped.Key,
                    Cars = (from car in grouped
                            orderby car.Combined descending
                            select car).Take(2)
                };

            foreach (var group in query4)
            {
                Console.WriteLine(group.Name);
                foreach (var car in group.Cars)
                {
                    Console.WriteLine($"\t{car.Name} : {car.Combined}");
                }
            }


        }

        private static void InsertData()
        {
            var cars = ProcessCars("fuel.csv");
            var db = new CarDb();

            if (!db.Cars.Any())
            {
                foreach (var car in cars)
                {
                    db.Cars.Add(car);
                }
            }
            db.SaveChanges();
        }

        private static List<Manufacturer> ProcessManufacturers(string path)
        {
            return File.ReadAllLines(path)
                       .Where(line => line.Length > 1)
                       .Select(line =>
                       {
                           var columns = line.Split(',');
                           return new Manufacturer
                           {
                               Name = columns[0],
                               Headquarters = columns[1],
                               Year = int.Parse(columns[2])
                           };
                       }).ToList();
        }

        private static List<Car> ProcessCars(string path)
        {
            return File.ReadAllLines(path)
                       .Skip(1)
                       .Where(line => line.Length > 1)
                       //.Select(Car.ParseFromCSV).ToList();
                       .ToCar().ToList();
        }
    }
}
